<?php namespace Trka\Postmaster\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Mail;
use Trka\Postmaster\Classes\WpExchange;
use Trka\Postmaster\Classes\WpGiftCard;

class Cards extends Controller
{
    public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\ReorderController'];

    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Trka.Postmaster', 'main-menu-item', 'side-menu-item');
    }

    public function onFetchCards()
    {
        $withrecip = $this->fetchCardsWithRecipient();
        $anonymous = $this->fetchCardsWithoutRecipient();

        return [
            'status' => 'processed',
            'count' => [
                'legacy' => $anonymous,
                'withmeta' => $withrecip
            ]
        ];
    }

    protected function fetchCardsWithRecipient()
    {
        $unclaimedCards = $this->fetchCardsSet(WpExchange::EXCHANGEFILTER_WITH_RECIP);
        foreach ($unclaimedCards as $card) {
            $this->mail_toGiftRecipient($card);
        }

        return count($unclaimedCards);
    }

    protected function fetchCardsWithoutRecipient()
    {
        $unclaimedCards = $this->fetchCardsSet(WpExchange::EXCHANGEFILTER_WITHOUT_RECIP);
        foreach ($unclaimedCards as $card) {
            $this->mail_legacyCard_toGiftBuyer($card);
        }

        return count($unclaimedCards);
    }

    protected function fetchCardsSet($filter)
    {
        $wpx = new WpExchange();
        return $wpx->getUnclaimedCards($filter);
    }

    protected function mail_toGiftRecipient(WpGiftCard $card)
    {
        $mailvars = [
            'buyer_fname' => $card->buyer->first_name,
            'buyer_lname' => $card->buyer->last_name,
            'buyer_email' => $card->buyer->email,
            'recip_fname' => $card->recipient->first_name,
            'recip_lname' => $card->recipient->last_name,
            'recip_email' => $card->recipient->email,
            'card_code' => $card->code_text,
            'card_register' => $card->register_url,
            'card_starts' => $card->starts,
        ];
        $currentmailer = $card;
        $currentmailer->recipient->email = 'kevin@riverworks.biz';

        Mail::queue('trka.postmaster::mail.message', $mailvars, function ($message) use ($currentmailer) {
            $message->to($currentmailer->recipient->email, $currentmailer->recipient->first_name . ' ' . $currentmailer->recipient->last_name);
        });
    }

    protected function mail_legacyCard_toGiftBuyer(WpGiftCard $card)
    {
        $mailvars = [
            'buyer_name' => $card->wpuser->displayname,
            'buyer_email' => $card->wpuser->email,
            'card_starts' => $card->starts,
            'card_code' => $card->code,
            'card_register' => 'https://thesewaneereview.com/subscriptions/checkout/?level=1&discount_code=' . $card->code,
        ];
        $currentmailer = $card;
        $currentmailer->wpuser->email = 'kevin@riverworks.biz';

        Mail::queue('trka.postmaster::mail.buyer-message', $mailvars, function ($message) use ($currentmailer) {
            $message->to($currentmailer->wpuser->email, $currentmailer->wpuser->displayname);
        });
    }

}