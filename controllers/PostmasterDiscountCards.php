<?php namespace Trka\Postmaster\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Flash;
use Mail;
use phpDocumentor\Parser\Exception;
use Trka\Postmaster\Classes\Schedulable;
use Trka\Postmaster\Classes\WpExchange;
use Trka\Postmaster\Classes\WpGiftCard;
use Trka\Postmaster\Models\PostmasterDiscountCard;
use Trka\Postmaster\Models\PostmasterEmailLog;

class PostmasterDiscountCards extends Controller
{
    public $implement = ['Backend\Behaviors\ListController', 'Backend\Behaviors\FormController', 'Backend\Behaviors\ReorderController'];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $cfg_registerlink_stub = 'https://thesewaneereview.com/subscriptions/checkout/?level=1&discount_code=';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Trka.Postmaster', 'main-menu-item', 'side-menu-cards');
    }


    //---------------------------- Ajax Handlers

    /**
     * Ajax Handler: Process outstanding WP cards into Postmaster
     * @return array
     */
    public function onRequestFetchCards()
    {
        $outcome = $this->doRequestFetchCards();
        $trans = [
            '%f' => $outcome['found'],
            '%s' => $outcome['skipped'],
            '%i' => $outcome['imported'],
        ];
        $message = strtr('Done. Processed from wordpress. Found %f total, imported %i, and skipped %s that were done previously. Please refresh the page.', $trans);

        Flash::success($message);
    }

    protected $mailstats = array(
        'success' => 0,
        'nomail' => 0,
        'badmail' => 0
    );

    /**
     * Ajax Handler: Send first mail
     *   - first mail: New card, no mail in log for it.
     */
    public function onRequestSendFirstMail()
    {
        $this->doSendFirstMail();
        $success = $this->mailstats['success'];
        $badmail = $this->mailstats['badmail'];
        $nomail = $this->mailstats['nomail'];

        $trans = [
            '%good' => $success,
            '%empty' => $nomail,
            '%inval' => $badmail,
        ];
        $message = strtr('Done. Sent %good emails. Please refresh the page.', $trans);
        Flash::success($message);
    }

    public function doRequestFetchCards()
    {
        $sched = new Schedulable();
        return $sched->fetchWpCardsAndFlush();
    }

    public function doSendFirstMail()
    {
        $cards = PostmasterDiscountCard::all();
        foreach ($cards as $card) {
            $isunclaimed = $this->isCardUnclaimed($card);
            // filter the email: Re-check if the card's been claimed or not, and - if not - send email. If it has already been claimed, delete the record from october.
            if ($isunclaimed === true) {
            $since_secs = strtotime($this->getNowSql()) - strtotime($card->last_mail);
            $since_days = $since_secs / (60 * 60 * 24);
            if ($since_days > 3 || is_null($card->last_mail)) {
                $this->mail_toGiftRecipient($card);
            }
            } else {
                $card->delete();
        }
    }
    }


    //---------------------------- Class Methods

    /**
     * Process wp card to Postmaster model instance
     * @param WpGiftCard $card
     */
    public function parse_wpCardToPostmaster(WpGiftCard $card)
    {
        if ($this->isCardRegistered($card->wp_id)) {
            return false;
        }

        $pmCard = new PostmasterDiscountCard();
        $pmCard->code = $card->code;
        $pmCard->wp_id = $card->wp_id;
        $pmCard->starts = $card->starts;
        $pmCard->expires = $card->expires;
        $pmCard->uses_remaining = $card->uses;

        $pmCard->buyer_fname = $card->buyer->first_name;
        $pmCard->buyer_lname = $card->buyer->last_name;
        $pmCard->buyer_email = $card->buyer->email;

        $pmCard->recip_fname = $card->recipient->first_name;
        $pmCard->recip_lname = $card->recipient->last_name;
        $pmCard->recip_email = $card->recipient->email;

        $pmCard->save();

        return true;
    }

    /**
     * Functional util: returns bool if the wp card is already registered here.
     * @param $wp_id
     *  the wordpress card id (pri. key)
     * @return bool
     */
    protected function isCardRegistered($wp_id)
    {
        $registered_count = PostmasterDiscountCard::where('wp_id', $wp_id)
            ->count();

        return $registered_count > 0;
    }

    /**
     * Sends mail for card
     * @param PostmasterDiscountCard $card
     */
    protected function mail_toGiftRecipient(PostmasterDiscountCard $card)
    {
        $mailvars = [
            'buyer_fname' => $card->buyer_fname,
            'buyer_lname' => $card->buyer_lname,
            'buyer_email' => $card->buyer_email,
            'recip_fname' => $card->recip_fname,
            'recip_lname' => $card->recip_lname,
            'recip_email' => $card->recip_email,
            'card_code' => $card->code,
            'card_register' => $this->cfg_registerlink_stub . $card->code,
            'card_starts' => $card->starts,
        ];

        $currentmailer = $card;
        $mailto = $currentmailer->recip_email;

        if ($this->isValidEmailAddress($card->recip_email) === 1) {
            Mail::send('trka.postmaster::mail.message', $mailvars, function ($message) use ($currentmailer, $mailto) {
                $out_status = $message->to($mailto, $currentmailer->recip_fname . ' ' . $currentmailer->recip_lname);
                $event_ts = $this->getNowSql();
                $log = new PostmasterEmailLog();
                $log->mailed_on = date("Y-m-d H:i:s");
                $log->mailed_to = $currentmailer->recip_email;
                $log->for_card()->add($currentmailer);
                $log->created_at = $event_ts;
                $log->updated_at = $event_ts;
                $log->save();

                $currentmailer->last_mail = $event_ts;
                $currentmailer->save();
                $this->mailstats['success'] += 1;
            });
        } elseif ($this->isValidEmailAddress($card->recip_email) === 0) {
            $this->mailstats['badmail'] += 1;
        } elseif ($this->isValidEmailAddress($card->recip_email) === false) {
            $this->mailstats['nomail'] += 1;
        }
    }

    //---------------------------- Utils
    protected function isCardUnclaimed(PostmasterDiscountCard $card)
    {
        // @todo use $card->wp_id to find the card's *current* available uses
        $wpx = new WpExchange();
        $updated = $wpx->getOneCard($card->wp_id);
        return $updated->uses > 0;
    }

    protected function getNowSql()
    {
        return date("Y-m-d H:i:s");
    }

    /**
     * Verifies the syntax of the given e-mail address.
     *
     * See RFC 2822 for details.
     *
     * @param $mail
     *   A string containing an e-mail address.
     *
     * @return
     *   1 if the email address is valid, 0 if it is invalid or empty, and FALSE if
     *   there is an input error (such as passing in an array instead of a string).
     */
    protected function isValidEmailAddress($mail)
    {
        $user = '[a-zA-Z0-9_\-\.\+\^!#\$%&*+\/\=\?\`\|\{\}~\']+';
        $domain = '(?:(?:[a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.?)+';
        $ipv4 = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
        $ipv6 = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';

        return preg_match("/^$user@($domain|(\[($ipv4|$ipv6)\]))$/", $mail);
    }

    //---------------------------- Deprecated

    /**
     * @deprecated
     */
    protected function fetchCardsWithoutRecipient()
    {
        $unclaimedCards = $this->fetchCardsSet(WpExchange::EXCHANGEFILTER_WITHOUT_RECIP);
        foreach ($unclaimedCards as $card) {
            $this->mail_legacyCard_toGiftBuyer($card);
        }

        return count($unclaimedCards);
    }

    /**
     * @deprecated
     */
    protected function mail_legacyCard_toGiftBuyer(WpGiftCard $card)
    {
        $mailvars = [
            'buyer_name' => $card->wpuser->displayname,
            'buyer_email' => $card->wpuser->email,
            'card_starts' => $card->starts,
            'card_code' => $card->code,
            'card_register' => 'https://thesewaneereview.com/subscriptions/checkout/?level=1&discount_code=' . $card->code,
        ];
        $currentmailer = $card;
        $currentmailer->wpuser->email = 'kevin@riverworks.biz';

        Mail::queue('trka.postmaster::mail.buyer-message', $mailvars, function ($message) use ($currentmailer) {
            $message->to($currentmailer->wpuser->email, $currentmailer->wpuser->displayname);
        });
    }
}