# Postmaster Plugin 

manages mailers for other systems (specifically: Wordpress). 

## installation

- follow the usual steps to install an oc plugin from git, minding the namespace. 
- in config/database.php, provide a config for the other db connection.
- using Builder or whatever method you <3, create the oc models, etc for what you're monitoring. 
- the classes dir is a good place for util classes that populate your models from raw sql. 


This **specific** impl. is setup for finding gift cards that were sold through wp+paid-membership-pro and managing the mailers and tpls by way of an hourly schedule, but you can adapt it however you want, of course. 

