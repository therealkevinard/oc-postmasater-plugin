<?php
namespace Trka\Postmaster\Classes;

use Db;

class WpExchange
{
    const EXCHANGEFILTER_WITH_RECIP = 'EXCHANGEFILTER_WITH_RECIP';
    const EXCHANGEFILTER_WITHOUT_RECIP = 'EXCHANGEFILTER_WITHOUT_RECIP';

    public function __construct()
    {
    }

    public function getPendingMails()
    {
    }

    public function getUnclaimedCards($filter)
    {
        $ret = array();

        switch ($filter) {
            case WpExchange::EXCHANGEFILTER_WITH_RECIP:
                foreach ($this->getAllUnclaimedSingleCards() as $card) {
                    if (!is_null($card->recipient)) {
                        $ret[] = $card;
                    }
                }
                break;
            case WpExchange::EXCHANGEFILTER_WITHOUT_RECIP:
                foreach ($this->getAllUnclaimedSingleCards() as $card) {
                    if (is_null($card->recipient)) {
                        $ret[] = $card;
                    }
                }
                break;
            default:
                $ret = $this->getAllUnclaimedSingleCards();
                break;
        }

        return $ret;
    }

    public function getAllUnclaimedSingleCards()
    {
        $ret = array();
        $cards = $this->getRemoteConnection()
            ->select('select * from wp_pmpro_discount_codes where uses = 1');
        foreach ($cards as $card) {
            $c = new WpGiftCard();
            $c->id = $card->id;
            $c->code = $card->code;
            $c->starts = $card->starts;
            $c->expires = $card->expires;
            $c->uses = $card->uses;
            $this->hydrateCard($c);
            $ret[] = $c;
        }
        return $ret;
    }

    /**
     * Fetch a single card from wp by id.
     *
     * @param $byid
     * @return bool
     */
    public function getOneCard($byid)
    {
        $card = $this->getRemoteConnection()
            ->select("select * from wp_pmpro_discount_codes where id=$byid");
        if (count($card) > 0) {
            return $card[0];
        } else {
            return false;
        }
    }

    public function hydrateCard(WpGiftCard $card)
    {
        //-- prepare for edge cases where we don't have recorded purchased_for meta
        $meta_rawWpUserId = $this->getRemoteConnection()
            ->select('SELECT user_id FROM `wp_usermeta` where meta_key = \'pmprogl_gift_codes_purchased\' AND meta_value LIKE "%i:' . $card->id . '%" LIMIT 1');
        $meta_rawWpUserInfo = $this->getRemoteConnection()
            ->select('SELECT ID, user_email, display_name from wp_users WHERE ID=' . $meta_rawWpUserId[0]->user_id);
        $card->wpuser = new WpCoreUser();
        $card->wpuser->email = $meta_rawWpUserInfo[0]->user_email;
        $card->wpuser->displayname = $meta_rawWpUserInfo[0]->display_name;
        $card->wpuser->userid = $meta_rawWpUserInfo[0]->ID;

        //-- try to hydrate the newer cards that have full meta
        $meta_cardGiftInfo = $this->getRemoteConnection()
            ->select('SELECT * FROM `wp_usermeta` where meta_key = \'pmpro_gift_codes_purchased_for\' AND meta_value LIKE "%' . $card->code . '%" LIMIT 1');
        if (count($meta_cardGiftInfo)) {
            $cardsarr = unserialize($meta_cardGiftInfo[0]->meta_value);
            foreach ($cardsarr as $cm) {
                if ($cm['code']['id'] == $card->id) {
                    $card->buyer = new WpContactPerson();
                    $card->buyer->first_name = $cm['buyer']['fname'];
                    $card->buyer->last_name = $cm['buyer']['lname'];
                    $card->buyer->email = $cm['buyer']['email'];

                    $card->recipient = new WpContactPerson();
                    $card->recipient->first_name = $cm['recipient']['fname'];
                    $card->recipient->last_name = $cm['recipient']['lname'];
                    $card->recipient->email = $cm['recipient']['email'];

                    $card->wp_id = $cm['code']['id'];
                    $card->code_text = $cm['code']['text'];
                    $card->register_url = $cm['code']['url'];
                }
            }
        }
        return $card;
    }

    private $dbconn = 'wp_external';
    private $remoteConnection = null;

    private function getRemoteConnection()
    {
        if (!$this->remoteConnection) {
            $this->remoteConnection = Db::connection($this->dbconn);
        }
        return $this->remoteConnection;
    }

    public function getAllPurchasedCardsByUser()
    {
        $ret = array();
        $usermeta = $this->getRemoteConnection()
            ->select('select * from wp_usermeta WHERE meta_key = ?', ['pmprogl_gift_codes_purchased']);
        foreach ($usermeta as $meta) {
            $personcard = new \stdClass();
            $personcard->userid = $meta->user_id;
            $personcard->purchasedcards = unserialize($meta->meta_value);
            $ret[] = $personcard;
        }
        return $ret;
    }


}