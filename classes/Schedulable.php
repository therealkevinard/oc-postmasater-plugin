<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 5/9/17
 * Time: 11:31 AM
 */

namespace Trka\Postmaster\Classes;


use Trka\Postmaster\Controllers\PostmasterDiscountCards;

class Schedulable
{

    /**
     * Import new gift cards
     * @return array
     *  metrics: found, skipped, and imported counts
     */
    public function fetchWpCardsAndFlush()
    {
        $pmcard_controller = new PostmasterDiscountCards();

        $metrics = [
            'found' => 0,
            'skipped' => 0,
            'imported' => 0,
        ];
        $unclaimedCards = $this->fetchCardsSet(WpExchange::EXCHANGEFILTER_WITH_RECIP);

        foreach ($unclaimedCards as $card) {
            $imported = $pmcard_controller->parse_wpCardToPostmaster($card);

            if ($imported) {
                $metrics['imported'] = $metrics['imported'] + 1;
            } elseif (!$imported) {
                $metrics['skipped'] = $metrics['skipped'] + 1;
            }
        }

        $metrics['found'] = count($unclaimedCards);

        return $metrics;
    }

    protected function fetchCardsSet($filter)
    {
        $wpx = new WpExchange();
        return $wpx->getUnclaimedCards($filter);
    }


    /**
     * Send emails for new cards.
     */
    public function sendFirstMailer()
    {
        $pmcard_controller = new PostmasterDiscountCards();
        return $pmcard_controller->doSendFirstMail();
    }
}