<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 4/26/17
 * Time: 4:14 PM
 */

namespace Trka\Postmaster\Classes;


class WpContactPerson
{
    public $first_name;
    public $last_name;
    public $email;
}