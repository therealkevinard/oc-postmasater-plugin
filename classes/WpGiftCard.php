<?php
/**
 * Created by IntelliJ IDEA.
 * User: kevin
 * Date: 4/26/17
 * Time: 1:36 PM
 */

namespace Trka\Postmaster\Classes;


class WpGiftCard
{
    public $id;
    public $code;
    public $starts;
    public $expires;
    public $uses;

    public $wpuser;
    public $buyer;
    public $recipient;
    public $wp_id;
    public $code_text;
    public $register_url;
}