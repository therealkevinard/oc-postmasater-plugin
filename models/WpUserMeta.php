<?php namespace Trka\Postmaster\Models;

use Model;

/**
 * WpUserMeta Model
 */
class WpUserMeta extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'wp_usermeta';
    protected $connection = "wp_external";
    protected $primaryKey = 'umeta_id';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'wpuser' => ['Trka\Postmaster\Models\WpUser', 'key'=>'user_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
