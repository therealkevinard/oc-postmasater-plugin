<?php namespace Trka\Postmaster\Models;

use Model;

/**
 * WpUser Model
 */
class WpUser extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'wp_users';
    protected $connection = "wp_external";
    protected $primaryKey = 'ID';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'wpusermeta' => ['Trka\Postmaster\Models\WpUserMeta', 'key'=>'umeta_id', 'otherKey'=>'user_id']
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
