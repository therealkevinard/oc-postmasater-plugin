<?php namespace Trka\Postmaster\Models;

use Model;

/**
 * Model
 */
class PostmasterEmailLog extends Model
{
    use \October\Rain\Database\Traits\SoftDelete;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'trka_postmaster_postmaster_email_log';

    protected $dates = ['deleted_at'];
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public $belongsTo = [
        'for_card' => 'Trka\Postmaster\Models\PostmasterDiscountCard'
    ];

}