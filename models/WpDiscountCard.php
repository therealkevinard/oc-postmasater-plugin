<?php namespace Trka\Postmaster\Models;

use Model;

/**
 * WpDiscountCard Model
 */
class WpDiscountCard extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'wp_pmpro_discount_codes';
    protected $connection = "wp_external";
    protected $primaryKey = 'id';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
