<?php namespace Trka\Postmaster\Models;

use Model;

/**
 * PostmasterDiscountCard Model
 */
class PostmasterDiscountCard extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'trka_postmaster_postmaster_discount_cards';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public $has_one = [
        'sent_mails' => 'Trka\Postmaster\Models\PostmasterEmailLog',
    ];


}
