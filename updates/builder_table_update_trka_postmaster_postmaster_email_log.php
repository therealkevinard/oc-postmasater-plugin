<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaPostmasterPostmasterEmailLog extends Migration
{
    public function up()
    {
        Schema::table('trka_postmaster_postmaster_email_log', function($table)
        {
            $table->renameColumn('for_card_id', 'trka_postmaster_postmaster_discount_cards');
        });
    }
    
    public function down()
    {
        Schema::table('trka_postmaster_postmaster_email_log', function($table)
        {
            $table->renameColumn('trka_postmaster_postmaster_discount_cards', 'for_card_id');
        });
    }
}
