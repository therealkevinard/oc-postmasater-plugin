<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateWpDiscountCardsTable extends Migration
{
    public function up()
    {
        Schema::create('trka_postmaster_wp_discount_cards', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('trka_postmaster_wp_discount_cards');
    }
}
