<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaPostmasterPostmasterDiscountCards2 extends Migration
{
    public function up()
    {
        Schema::table('trka_postmaster_postmaster_discount_cards', function($table)
        {
            $table->date('starts')->nullable();
            $table->date('expires')->nullable();
            $table->integer('uses_remaining')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('trka_postmaster_postmaster_discount_cards', function($table)
        {
            $table->dropColumn('starts');
            $table->dropColumn('expires');
            $table->dropColumn('uses_remaining');
        });
    }
}
