<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTrkaPostmasterPostmasterEmailLog extends Migration
{
    public function up()
    {
        Schema::create('trka_postmaster_postmaster_email_log', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->dateTime('mailed_on')->nullable();
            $table->text('mailed_to')->nullable();
            $table->integer('for_card_id')->unsigned();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('trka_postmaster_postmaster_email_log');
    }
}
