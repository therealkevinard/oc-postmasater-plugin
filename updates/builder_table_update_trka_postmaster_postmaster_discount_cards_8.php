<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaPostmasterPostmasterDiscountCards8 extends Migration
{
    public function up()
    {
        Schema::table('trka_postmaster_postmaster_discount_cards', function($table)
        {
            $table->dateTime('last_mail')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('trka_postmaster_postmaster_discount_cards', function($table)
        {
            $table->dropColumn('last_mail');
        });
    }
}
