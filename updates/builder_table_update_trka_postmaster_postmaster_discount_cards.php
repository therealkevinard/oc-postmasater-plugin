<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaPostmasterPostmasterDiscountCards extends Migration
{
    public function up()
    {
        Schema::rename('trka_postmaster_cards', 'trka_postmaster_postmaster_discount_cards');
    }
    
    public function down()
    {
        Schema::rename('trka_postmaster_postmaster_discount_cards', 'trka_postmaster_cards');
    }
}
