<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaPostmasterPostmasterEmailLog4 extends Migration
{
    public function up()
    {
        Schema::table('trka_postmaster_postmaster_email_log', function($table)
        {
            $table->renameColumn('postmaster_postmaster_discount_card_id', 'postmaster_discount_card_id');
        });
    }
    
    public function down()
    {
        Schema::table('trka_postmaster_postmaster_email_log', function($table)
        {
            $table->renameColumn('postmaster_discount_card_id', 'postmaster_postmaster_discount_card_id');
        });
    }
}
