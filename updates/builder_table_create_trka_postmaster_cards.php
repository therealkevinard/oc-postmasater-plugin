<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTrkaPostmasterCards extends Migration
{
    public function up()
    {
        Schema::create('trka_postmaster_cards', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('code', 32)->nullable();
            $table->integer('wp_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('trka_postmaster_cards');
    }
}
