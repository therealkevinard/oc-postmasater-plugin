<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaPostmasterPostmasterDiscountCards3 extends Migration
{
    public function up()
    {
        Schema::table('trka_postmaster_postmaster_discount_cards', function($table)
        {
            $table->string('buyer_fname', 64)->nullable();
            $table->string('buyer_lname', 64)->nullable();
            $table->string('buyer_email', 64)->nullable();
            $table->integer('buyer_id')->nullable();
            $table->string('recip_fname', 64)->nullable();
            $table->string('recip_lname', 64)->nullable();
            $table->string('recip_email', 64)->nullable();
            $table->integer('recip_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('trka_postmaster_postmaster_discount_cards', function($table)
        {
            $table->dropColumn('buyer_fname');
            $table->dropColumn('buyer_lname');
            $table->dropColumn('buyer_email');
            $table->dropColumn('buyer_id');
            $table->dropColumn('recip_fname');
            $table->dropColumn('recip_lname');
            $table->dropColumn('recip_email');
            $table->dropColumn('recip_id');
        });
    }
}
