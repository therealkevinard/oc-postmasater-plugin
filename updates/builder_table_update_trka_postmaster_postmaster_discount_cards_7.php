<?php namespace Trka\Postmaster\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTrkaPostmasterPostmasterDiscountCards7 extends Migration
{
    public function up()
    {
        Schema::table('trka_postmaster_postmaster_discount_cards', function($table)
        {
            $table->renameColumn('postmaster_email_log_id', 'sent_mails_id');
        });
    }
    
    public function down()
    {
        Schema::table('trka_postmaster_postmaster_discount_cards', function($table)
        {
            $table->renameColumn('sent_mails_id', 'postmaster_email_log_id');
        });
    }
}
