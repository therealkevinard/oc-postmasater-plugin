<?php namespace Trka\Postmaster;

use System\Classes\PluginBase;
use Trka\Postmaster\Classes\Schedulable;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {
            $sc = new Schedulable();
            $sc->fetchWpCardsAndFlush();
            $sc->sendFirstMailer();
        })
            ->hourly();
    }

    public function registerMailTemplates()
    {
        return [
            'trka.postmaster::mail.message' => 'Standard message for Gift Card recipient.'
        ];
    }
}
