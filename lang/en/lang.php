<?php return [
    'plugin' => [
        'name' => 'Postmaster',
        'description' => 'Outside observer for independently managing mail-outs from other systems.'
    ]
];